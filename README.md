# Available tasks
Hello, here is testing round tasks for Frontend School in T-Systems.

## Vera Nikitinskaya
nikitinskaya.v@gmail.com

+7 (921) 448-60-10

## 1 Broken page
This nice mosaic was totally broken, please fix it.
- [Fixed page](broken-page/index.html)

## 2 Maze
Help orange square get out of the frightening maze.

Unfortunately, the orange square doesn't know how to make moves up and left.
You have to teach it to do that.

- [Square out of the maze](maze/index.html)

## 3 Page from JPEG
We've lost sources of our main page, only one last screenshot was left.

Please help us to compose this web-page again.

- [Source files](page-from-jpeg)

## 4 Algorithm in JS
Fibonacci has called. Seems he can’t recall his numbers, except for the first two: 1, 1.

Could you create .js file, that Fibonacci can copypaste in his browser console and get any Nth number by inputing the N into it.
- [Recursive](fibonacci/recursive.js)
- [Iterative](fibonacci/iterative.js)

## How to pass
Make a fork from this repository, complete tasks and add your contact info for next communication.

And one more thing: be brave and creative :)
We are waiting for you in our team!
