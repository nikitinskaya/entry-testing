/* Though not as convenient to copypaste as
the recursive one-liner,
Mr. Fibonacci may use this solution for larger N
since it requires linear time only. */

fib = n => {
  let prev = 1,
    next = 1,
    acc = 0,
    i = 0;

  while(i < n) {
    acc = prev;
    prev = next;
    next = acc + prev;
    i++;
  }

  return acc;
}

let N = 6;

fib(N);
