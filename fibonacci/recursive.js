/* Mr. Fibonacci could use this intuitive solution,
though on small numbers only if he is in a hurry
or doesn't want to encounter a stack overflow. */

fib = n => n <= 1 ? n : fib(n-1) + fib(n-2);

let N = 6;

fib(N);
